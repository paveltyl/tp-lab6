//
//  SecondViewController.swift
//  Calory Calculator
//
//  Created by Pavel Tyletsky on 14/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {
    @IBOutlet weak var fatsTextField: UITextField!
    @IBOutlet weak var proteinsTextField: UITextField!
    @IBOutlet weak var carbohydratesTextField: UITextField!
    @IBOutlet weak var resultLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func kilocalories(fats: Double, proteins: Double, carbohydrates: Double) -> Double {
        return 4 * (carbohydrates + proteins) + 9 * fats
    }
    
    @IBAction func solveButton(_ sender: Any) {
        var kc: Double = 0
        if let fats: Double = Double(fatsTextField.text!) {
            if let proteins: Double = Double(proteinsTextField.text!) {
                if let carbohydrates: Double = Double(carbohydratesTextField.text!) {
                    kc = kilocalories(fats: fats, proteins: proteins, carbohydrates: carbohydrates)
                }
            }
        }
        resultLabel.text = "Указанные вещества \n содержат \(kc) ккал"
        UIApplication.shared.keyWindow!.endEditing(true)
    }

}

