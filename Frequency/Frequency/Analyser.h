//
//  Analizer.h
//  Frequency
//
//  Created by Pavel Tyletsky on 13/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Analyser : NSObject

- (NSArray*)getFirst:(unsigned long)count ForText:(NSString *)text;

@end
