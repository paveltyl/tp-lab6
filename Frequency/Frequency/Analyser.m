//
//  Analizer.m
//  Frequency
//
//  Created by Pavel Tyletsky on 13/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import "Analyser.h"

@implementation Analyser

- (NSArray*)getFirst:(unsigned long)count ForText:(NSString *)text
{
    NSArray *words = [text componentsSeparatedByString:@" "];
    NSMutableDictionary *statistics = [NSMutableDictionary dictionary];
    NSNumber *repetitions;
    
    for (NSString* word in words) {
        repetitions = [statistics valueForKey:word];
        [statistics setObject:[[NSNumber alloc] initWithLong: ([repetitions integerValue] + 1)] forKey:word];
    }
    
    NSArray *orderedKeys = [statistics keysSortedByValueUsingComparator:^NSComparisonResult(id obj1, id obj2){
        return [obj2 compare:obj1];
    }];
    
    return [orderedKeys subarrayWithRange:NSMakeRange(0, count)];
}
@end
