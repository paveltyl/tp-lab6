//
//  main.m
//  Frequency
//
//  Created by Pavel Tyletsky on 13/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Analyser.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        Analyser *analyser = [[Analyser alloc] init];
        NSArray *arr = [analyser getFirst:5 ForText:@"Hello World jejkr eurfheiurh Hello World Hello fge"];
        for(NSString* word in arr){
            NSLog(@"%@", word);
        }
    }
    return 0;
}
