//
//  AppDelegate.h
//  Weather Forecast
//
//  Created by Pavel Tyletsky on 13/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

