//
//  ViewController.m
//  Weather Forecast
//
//  Created by Pavel Tyletsky on 13/4/17.
//  Copyright © 2017 Pavel Tyletsky. All rights reserved.
//

#import "ViewController.h"
#import "LocationManager.h"
#define KelvinToCelcium 273.15
#define APPID @"71ead9fc849b65dfe5370001a537728c"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *temperature;
@property (weak, nonatomic) IBOutlet UIButton *check;
@end
LocationManager *loc;

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    loc = [[LocationManager alloc] init];
    [loc startLocating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)UpdateWeather:(id)sender {
    NSArray *curLoc = [loc getLocations];
    NSURL *url = [NSURL URLWithString:
                  [NSString stringWithFormat:
                  @"http://api.openweathermap.org/data/2.5/weather?lat=%@&lon=%@&appid=%@",
                  curLoc[0], curLoc[1], APPID]];
    NSData *data = [NSData dataWithContentsOfURL:url];
    NSError *error = nil;
    NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options: NSJSONReadingMutableContainers error:&error];
    double temp = [[[response objectForKey: @"main"] objectForKey:@"temp"] doubleValue];
    NSNumber *tempC = [NSNumber numberWithDouble:temp - KelvinToCelcium];
    [[self temperature] setText:[NSString stringWithFormat:@"%.1f", [tempC doubleValue]]];
}

@end
